import { Routes } from '@angular/router';
import { LaunchListComponent } from '../components/launch-list/launch-list.component';
import { LaunchDetailsComponent } from '../components/launch-details/launch-details.component';

export const routes: Routes = [
  {
    path: '',
    component: LaunchListComponent,
  },
  {
    path: ':id',
    component: LaunchDetailsComponent,
  },
];
